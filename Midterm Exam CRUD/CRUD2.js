var app = new function() {
    this.el = document.getElementById('user');
    this.users = [];
    this.birth = [];
    this.ages = [];
    this.gender = [];
    this.Count = function(data) {
        var el = document.getElementById('counter');
        var name = 'User';

        if (data) {
            if (data > 1) {
                name = 'Users';
            }
            el.innerHTML = data + ' ' + name;
        } else {
            el.innerHTML = 'No ' + name + ' Found';
        }
    };

    this.FetchAll = function() {
        var data = '';

        if (this.users.length > 0) {
            for (i = 0; i < this.users.length; i++) {
                data += '<tr>';
                data += '<td>' + this.users[i] + '</td>';
                data += '<td>' + this.birth[i] + '</td>';
                data += '<td id="edage">' + this.ages[i] + '</td>';
                data += '<td>' + this.gender[i] + '</td>';
                data += '<td><button onclick="app.Edit(' + i + ')">Edit</button><button onclick="app.Delete(' + i + ')">Delete</button></td>';
                data += '</tr>';
            }
        }
        this.Count(this.users.length);
        return this.el.innerHTML = data;
    };

    this.Add = function() {
        nam = document.getElementById('add-name');
        old = document.getElementById('add-date');
        gen = document.getElementById('gender');
        var empty = false;
        // Get the value
        var user = nam.value;
        var date = old.value;
        var gend = gen.value;

        var arr = date.split("-");
        var year = arr[0];
        var month = arr[1];
        var day = arr[2];
        var now = new Date()
        var aged = now.getFullYear() - year;
        var mdif = now.getMonth() - month + 1;
        if (mdif < 0) {
            --aged;
        } else if (mdif == 0) {
            var ddif = now.getDate() - day;
            if (ddif < 0) {
                --aged;
            }
        }
        var age = parseInt(aged);

        if (age) {
            this.ages.push(age);
            this.FetchAll()
        }

        if (user) {
            // Add the new value
            this.users.push(user.trim());
            // Reset input value
            nam.value = '';
            // Dislay the new list
            this.FetchAll();
        } else if (user === '') {
            alert("Please Enter User name");
            empty = true;
        }
        if (date) {
            this.birth.push(date.trim());
            old.value = '';
            this.FetchAll();
        } else if (date === '') {
            alert("Please Enter Date of Birth");
            empty = true;
        }
        if (gend) {
            this.gender.push(gend.trim());
            gen.value = '';
            this.FetchAll();
        } else if (gend === '') {
            alert("Please Enter Gender");
            empty = true;
        }
    };

    this.Edit = function(item) {
        var editn = document.getElementById('edit-name');
        var editd = document.getElementById('edit-date');
        var editg = document.getElementById('edit-gender');
        //var editn = document.getElementById('add-name');
        //var editd = document.getElementById('add-date');
        //var editg = document.getElementById('gender');

        // Display value in the field
        editn.value = this.users[item];
        editd.value = this.birth[item];
        editg.value = this.gender[item];
        var newage = this.ages[item];

        console.log(newage);
        console.log(editd.value);

        // Display fields
        document.getElementById('spoiler').style.display = 'block';
        self = this;
        document.getElementById('saveEdit').onsubmit = function() {
            // Get value
            var name = editn.value;
            var date = editd.value;
            var gend = editg.value;
            var aged = newage;

            console.log(date);

            //var arr = aged.split("-");
            //var year = arr[0];
            //var month = arr[1];
            //var day = arr[2];


            if (name) {
                // Edit value
                var arr = date.split("-");
                var year = arr[0];
                var month = arr[1];
                var day = arr[2];
                var now = new Date()
                var aged = now.getFullYear() - year;
                var mdif = now.getMonth() - month + 1;
                if (mdif < 0) {
                    --aged;
                } else if (mdif == 0) {
                    var ddif = now.getDate() - day;
                    if (ddif < 0) {
                        --aged;
                    }
                }
                var age = parseInt(aged);

                console.log(age);

                self.ages.splice(item, 1, age);
                self.users.splice(item, 1, name.trim());
                self.birth.splice(item, 1, date.trim());
                self.gender.splice(item, 1, gend.trim());

                // Display the new list
                self.FetchAll();
                // Hide fields
                CloseInput();
            }
        }
    };

    this.Delete = function(item) {
        // Delete the current row
        this.users.splice(item, 1);
        // Display the new list
        this.FetchAll();
    };
}
app.FetchAll();

function CloseInput() {
    document.getElementById('spoiler').style.display = 'none';
}